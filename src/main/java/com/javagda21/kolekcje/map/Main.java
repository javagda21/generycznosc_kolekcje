package com.javagda21.kolekcje.map;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        // String - key
        // Integer - value
//        HashMap<String, Integer> map = new HashMap<>(); // NIE PISZEMY
//        Map<String, Integer> map = new HashMap<>(); // COOL :)

        Map<Integer, String> map = new HashMap<>(); // COOL :)
        List<String> lista = new ArrayList<>();
        lista.add("51");
        lista.add(0, "52");
        lista.add(0, "53");
        lista.add(0, "54");
        lista.add(0, "55");

        System.out.println(lista);

        map.put(101, "siema");
        map.put(100, "pawel");
        map.put(102, "pawel");
        System.out.println(map);

        if (map.containsKey(100L)) {
            String wynik = map.get(100L);
            System.out.println(wynik);
        }

        if (map.containsKey(100)) {
            String wynik = map.get(100);
            System.out.println(wynik);
        }

        map.remove(100);

        for (Integer klucz : map.keySet()) {
            System.out.println(klucz);
            System.out.println(klucz + " -> " + map.get(klucz));
        }

        for (String wartosc : map.values()) {
            System.out.println("Wartość: " + wartosc);
        }

        System.out.println();
        for (Map.Entry<Integer, String> wpis : map.entrySet()) {
            System.out.println(wpis.getKey() + " -> " + wpis.getValue());
        }

        // Lewa strona - SET
        // Prawa strona - "lista"
        // klucz -> hash -> index tablicy -> wartość tablicy to wartość mapyp

    }
}
