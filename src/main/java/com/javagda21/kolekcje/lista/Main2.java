package com.javagda21.kolekcje.lista;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
//import java.awt.List; // bebe - złe


public class Main2 {
    public static void main(String[] args) {
//        Obywatel o = new Król();
//        Ptak p = new Kukułka();
        // Zasada Liskov'a -
//        List<Integer> list = new ArrayList<>();
//        List<Integer> list = Arrays.asList(12, 2, 3, 4,5,436,457);
        // lista unmutable - nie można jej zmieniać
        List<Integer> list = new ArrayList<>(Arrays.asList(12, 2, 3, 4,5,436,457));
        // konstruktor kopiujący - kopiuje elementy z listy Arrays.asList do ArrayList
        System.out.println(list);

        list.add(5);
        System.out.println(list);

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        for (Integer integer : list) {
            System.out.println(integer);
        }

    }
}
