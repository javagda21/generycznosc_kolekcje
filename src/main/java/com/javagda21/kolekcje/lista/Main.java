package com.javagda21.kolekcje.lista;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> lista = new ArrayList<>();

        lista.add(5);
        lista.add(6);
        lista.add(7);
        lista.add(8);
        lista.add(9);
        lista.add(10);
        lista.add(8);
        lista.add(8);

        System.out.println(lista);
        lista.remove(4);
        System.out.println(lista);

        lista.remove((Object)8);
        System.out.println(lista);

    }
}
