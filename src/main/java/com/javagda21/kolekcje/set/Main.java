package com.javagda21.kolekcje.set;

import java.util.*;

public class Main {
    public static void main(String[] args) {
//        HashSet<Integer> integers = new HashSet<>(); // NIE!
        Set<Integer> integers = new HashSet<>(); // Tak :)

        integers.add(5);
        integers.add(4);
        integers.add(3);
        integers.add(2);
        integers.add(5);

        System.out.println(integers);

        String zdanie = "ala ma kota ale ala to ala i ala nie lubi kota";
        String[] słowa = zdanie.split(" ");

        Set<String> słowaWSecie = new HashSet<>();
        for (String s : słowa) {
            słowaWSecie.add(s);
        }

        System.out.println(słowaWSecie);

        List<Integer> liczby = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 6, 7, 8, 8, 89, 12, 1, 2, 3, 5, 6, 7));
        System.out.println(liczby);

//        Set<Integer> uzycieKonstruktoraKopijacego = new HashSet<>(liczby);
        Set<Integer> uzycieKonstruktoraKopijacego = new TreeSet<>(liczby);
        System.out.println(uzycieKonstruktoraKopijacego);

        Scanner scanner = new Scanner(System.in);
        Set<Integer> liczbyOdUzytkownika = new HashSet<>();

        do {
            System.out.println("Podaj liczbe:");
            liczbyOdUzytkownika.add(scanner.nextInt());
        }while (liczbyOdUzytkownika.size() < 6);

        System.out.println(liczbyOdUzytkownika);

    }
}
