package com.javagda21.kolekcje.set;

import java.util.HashSet;
import java.util.Set;

public class Main2 {
    public static void main(String[] args) {
        Set<Osoba> set = new HashSet<>();

        set.add(new Osoba("Marian"));
        set.add(new Osoba("Marian"));
        set.add(new Osoba("Marian"));
        set.add(new Osoba("Marian"));
        set.add(new Osoba("Marian"));
        set.add(new Osoba("Marian"));
        set.add(new Osoba("Marian"));

        System.out.println(set);

        for (Osoba osoba : set) {
            System.out.println(osoba);
        }
        System.out.println();
        for (Osoba osoba : set) {
            System.out.println(osoba);
        }

    }
}
