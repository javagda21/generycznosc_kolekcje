package com.javagda21.generycznosc.przyklad;

public class Funkcje {

    public static <T extends Number> double sumuj(T jednaLiczba, T drugaLiczba){
        return jednaLiczba.doubleValue() + drugaLiczba.doubleValue();
    }

    public static <T> T zworcWartosc(T costam){
        return costam;
    }

    public static <T> Para<T, T>[] znajdzNiepuste(Para<T, T>[] pary) {
        int licznik = 0;
        for (Para<T, T> ttPara : pary) {
            if (ttPara.czyPusta()) {
                licznik++;
            }
        }

        Para<T, T>[] pełne = new Para[licznik];
        licznik = 0;
        for (int i = 0; i < pary.length; i++) {
            if (!pary[i].czyPusta()) {
                pełne[licznik++] = pary[i];
            }
        }

        return pełne;
    }
}
