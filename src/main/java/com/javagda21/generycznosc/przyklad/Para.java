package com.javagda21.generycznosc.przyklad;

public class Para<P, L> {
    private P prawa;
    private L lewa;

    public Para(P prawa, L lewa) {
        this.prawa = prawa;
        this.lewa = lewa;
    }

    public P getPrawa() {
        return prawa;
    }

    public void setPrawa(P prawa) {
        this.prawa = prawa;
    }

    public L getLewa() {
        return lewa;
    }

    public void setLewa(L lewa) {
        this.lewa = lewa;
    }

    public boolean czyPusta() {
        boolean czyLewaPusta = lewa == null;
        boolean czyPrawaPusta = prawa == null;

        return czyLewaPusta && czyPrawaPusta;
    }

    @Override
    public String toString() {
        return "Para{" +
                "prawa=" + prawa +
                ", lewa=" + lewa +
                '}';
    }
}
