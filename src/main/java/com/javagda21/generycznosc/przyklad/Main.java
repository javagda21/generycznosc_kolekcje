package com.javagda21.generycznosc.przyklad;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
//        Para para = new Para(new Kot(), new Miś());
//
//        if(para.getPrawa() instanceof Samochód) {
//            Samochód zmienna = (Samochód) para.getPrawa();
//            zmienna.przyspiesz();
//        }
//        ########################################
//        Para<Kot> para = new Para<Kot>(new Kot(), new Kot());
//
//        para.getPrawa();
//
//        Para<Integer> para2;
//        para2.
//        ########################################
        Para<Kot, Miś> para = new Para<>(new Kot(), new Miś());
//        Para<int, Miś> para = new Para<>(5, new Miś()); // < - nie działa
//        Para<Integer, Miś> para = new Para<>(5, new Miś()); // działa

//        Number
//        znajdzNiepuste();
        Para<String, String> p1 = new Para<>("a", null);
        Para<String, String> p2 = new Para<>(null, "b");
        Para<String, String> p3 = new Para<>(null, null);
        Para<String, String> p4 = new Para<>("e", "b");
        Para<String, String> p5 = new Para<>("f", "b");
        Para<String, String> p6 = new Para<>(null, null);
        Para<String, String> p7 = new Para<>(null, "b");
        Para<String, String> p8 = new Para<>("i", "b");
        Para<String, String> p9 = new Para<>("j", null);

        Para<String, String>[] pary = new Para[]{p1, p2, p3, p4, p5, p6, p7, p8, p9};

        Para<String, String>[] paryNiepuste = Funkcje.znajdzNiepuste(pary);
        System.out.println(Arrays.toString(paryNiepuste));

        Integer wartosc = Funkcje.zworcWartosc(23);


        double suma = Funkcje.sumuj(5, 23);
        double suma2 = Funkcje.sumuj(23L, 2524234);

    }

}
